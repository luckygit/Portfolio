<?php
if(!isset($_POST['submit']))
{
	//This page should not be accessed directly. Need to submit the form.
	echo "error; you need to submit the form!";
}

$firstName = $_REQUEST['first_name'];
$lastName = $_REQUEST['last_name'];
$email = $_REQUEST['email'];
$message = $_REQUEST['message'];

//Validate first
if(empty($firstName)||empty($lastName)||empty($email) || empty($message))
{
    echo "Please fill out all of the fields!";
    exit;
}

if(IsInjected($visitor_email))
{
    echo "Bad email value!";
    exit;
}

$email_from = '$email';
$email_subject = "Portfolio site form";
$email_body = "New email from: $firstName $lastName\n".
"Their email is: $email \n".
"Their message is: $message \n".

$to = "lewisluckhurst@icloud.com";

mail($to,$email_subject,$email_body);
header('Location: index.html');

// Function to validate against any email injection attempts
function IsInjected($str)
{
  $injections = array('(\n+)',
              '(\r+)',
              '(\t+)',
              '(%0A+)',
              '(%0D+)',
              '(%08+)',
              '(%09+)'
              );
  $inject = join('|', $injections);
  $inject = "/$inject/i";
  if(preg_match($inject,$str))
    {
    return true;
  }
  else
    {
    return false;
  }
}
?>