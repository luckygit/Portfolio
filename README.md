# Portfolio
This is my personal website.

Currently my portfolio site only has one page, however I am working on creating a page for my personal project Simple PC builds, which can also be found on my GitHub page. I am working on this website in my spare time outside of university, I am using it as a method to refine my existing skills and teach myself new skills at the same time. 

If you find anything wrong with my website please alert me and do the same if you feel something could be improved any feedback is good feedback. 
